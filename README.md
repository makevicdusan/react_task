#**Game in react**

This is a test used for the assessment of the react.js front-end developer position.

#**Download**

```
cd /path_to_file_where_you_want_to_clone
```
```
git clone https://makevicdusan@bitbucket.org/makevicdusan/react_task.git
```

#**Installation**

```
cd react_task
```
```
npm install
```

#**Run application**

```
npm start
```
