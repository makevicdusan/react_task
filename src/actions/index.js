export const setBoxes = payload => ({
    type: 'set',
    payload,
});

export const changeStatus = payload => ({
    type: 'changeStatus',
    payload,
});
export const changeLevel = payload => ({
    type: 'changeCurrentLevel',
    payload,
});

export const chooseLevel = payload => ({
    type: 'chooseLevel',
    payload,
});

export const addClick = payload => ({
    type: 'addClick',
    payload,
});

export const addTimer = () => ({
    type: 'add',
});
export const resetTimer = () => ({
    type: 'reset',
});

export const addInterval = payload => ({
    type: 'setInterval',
    payload,
});
export const removeInterval = () => ({
    type: 'clearInterval',
});

export const addLife = () => ({
    type: 'addLife',
});

export const getUsersFromStorage = () => ({
    type: 'get',
});

export const changeUsersPlaylist = payload => ({
    type: 'change',
    payload,
});

export const addNewUserToList = payload => ({
    type: 'save',
    payload,
});

export const saveUserToStorage = payload => ({
    type: 'saveToStorageNew',
    payload,
});

export const saveExistingUserToStorage = payload => ({
    type: 'saveToStorageExisting',
    payload,
});

export const fetchUserFromStorage = payload => ({
    type: 'fetchUserFromStorage',
    payload,
});
export const minusLifes = payload => ({
    type: 'minusLifes',
    payload,
});

export const addScore = payload => ({
    type: 'addScore',
    payload,
});

export const levelScoresIsOpen = payload => ({
    type: 'levelScoresIsOpen',
    payload,
});
export const clearScores = payload => ({
    type: 'clearScores',
    payload,
});

export const selectLevel = payload => ({
    type: 'selectLevel',
    payload,
});
