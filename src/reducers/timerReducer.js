export default function timer(state = 0, action) {
    switch (action.type) {
    case 'add':
        state += 1; // eslint-disable-line
        return state;
    case 'reset':
        state = 0; // eslint-disable-line
        return state;
    default:
        return state;
    }
}
