export const Status = {
    READY: 0,
    RUNNING: 1,
    FINISHED: 2,
    PAUSED: 3,
    WIN: 4,
    LOSE: 5,
    GAMEOVER: 6,
    SCORE: 7,
};

const game = {
    status: Status.READY,
    level: 1,
};

export default function counter(state = game, action) {
    switch (action.type) {
    case 'changeStatus':
        return {
            ...state,
            status: action.payload,
        };
    case 'chooseLevel':
        return {
            ...state,
            level: action.payload,
        };
    default:
        return state;
    }
}
