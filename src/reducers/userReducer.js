let fromStorage;
let user;
let toStorage;
let toStorage2;

export default function userActions(state = {}, action) {
    switch (action.type) {
    case 'fetchUserFromStorage':
        fromStorage = localStorage.getItem(`react_task_user_${action.payload}`);
        state = JSON.parse(fromStorage); // eslint-disable-line
        let { scores } = state; // eslint-disable-line
        for (let i = 0; i < scores.length; i += 1) {
            scores[i].isOpen = false;
            scores[i].isSelected = false;
        }
        scores = scores.filter(level => (level.games.length > 0));
        return {
            ...state,
        };
    case 'saveToStorageNew':
        user = {
            id: action.payload.id,
            name: action.payload.name,
            currentLevel: 1,
            lifes: 0,
            scores: [],
        };
        state = user; // eslint-disable-line
        toStorage = JSON.stringify(user);
        if (toStorage.length > 5) {
            localStorage.setItem(`react_task_user_${user.id}`, toStorage);
        }
        return {
            ...state,
        };
    case 'changeCurrentLevel':
        if (state.currentLevel > action.payload) {
            return {
                ...state,
            };
        }
        return {
            ...state,
            currentLevel: action.payload,
        };

    case 'addScore':
        if (!state.scores[action.payload.level - 1]) {
            state.scores[action.payload.level - 1] = {
                level: action.payload.level,
                games: [],
            };
        }
        state.scores[action.payload.level - 1].games.push(action.payload.gameScore);
        return {
            ...state,
        };
    case 'addLife':
        state.lifes += 1;
        return {
            ...state,
        };
    case 'minusLifes':
        state.lifes -= action.payload;
        if (state.lifes < 0) {
            state.lifes = 0;
        }
        return {
            ...state,
        };
    case 'saveToStorageExisting':
        toStorage2 = JSON.stringify(state);
        if (toStorage2.length > 5) {
            localStorage.setItem(`react_task_user_${state.id}`, toStorage2);
        }
        return {
            ...state,
        };
    case 'levelScoresIsOpen':
        for (let i = 0; i < state.scores.length; i += 1) {
            if (state.scores[i] === action.payload.levelScores) {
                state.scores[i].isOpen = action.payload.isOpen;
            }
        }
        return {
            ...state,
        };
    case 'clearScores':

        let scores1 = action.payload.scores; // eslint-disable-line
        for (let i = 0; i < scores1.length; i += 1) {
            scores1[i].isOpen = false;
            scores1[i].games = scores1[i].games.filter(game => (game && game.timeLast !== null));
        }
        scores1 = scores1.filter(level => (level.games.length > 0));
        return {
            ...state,
        };
    case 'selectLevel':
        for (let i = 0; i < state.scores.length; i += 1) {
            state.scores[i].isSelected = false;
        }
        state.scores[action.payload].isSelected = true;
        return {
            ...state,
        };
    default:
        return state;
    }
}
