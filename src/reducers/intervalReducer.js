export default function interval(state = null, action) {
    switch (action.type) {
    case 'setInterval':
        state = action.payload; // eslint-disable-line
        return state;
    case 'clearInterval':
        clearInterval(state);
        return state;
    default:
        return state;
    }
}
