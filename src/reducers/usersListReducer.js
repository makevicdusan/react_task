let user;
let toStorage;
let fromStorage;

export default function list(state = [], action) {
    switch (action.type) {
    case 'add':
        state.push(action.payload);
        return [
            ...state,
        ];
    case 'save':
        user = {
            id: (state.length + 1),
            name: action.payload,
        };
        state.push(user);
        toStorage = JSON.stringify(state);
        localStorage.setItem('react_task_users', toStorage);
        return [
            ...state,
        ];
    case 'get':
        fromStorage = JSON.parse(localStorage.getItem('react_task_users'));
        state = Array.isArray(fromStorage) ? fromStorage : []; // eslint-disable-line
        for (let i = 0; i < state.length; i += 1) {
            state[i].isSelected = false;
        }
        return [
            ...state,
        ];
    case 'change':
        state = action.payload; // eslint-disable-line
        return [
            ...state,
        ];
    default:
        return state;
    }
}
