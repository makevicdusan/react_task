import { combineReducers } from 'redux';
import user from './userReducer';
import boxes from './boxesLeftReducer';
import timer from './timerReducer';
import game from './gameReducer';
import interval from './intervalReducer';
import users from './usersListReducer';

export default combineReducers({
    user,
    boxes,
    timer,
    game,
    interval,
    users,
});
