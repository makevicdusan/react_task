import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Main from './components/Main';
import NotFound from './components/NotFound';
import reducers from './reducers/index';

const store = createStore(reducers);

class App extends Component {
    render() { // eslint-disable-line
        return (
            <div className='app'>
                <Provider store={store}>
                    <BrowserRouter>
                        <Switch>
                            <Route path='/' component={Main}/>
                            <Route component={NotFound}/>
                        </Switch>
                    </BrowserRouter>
                </Provider>
            </div>
        );
    }
}

export default App;
