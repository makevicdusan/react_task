import React from 'react';
import '../css/Players.css';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    getUsersFromStorage, changeUsersPlaylist, addNewUserToList, saveUserToStorage,
    fetchUserFromStorage,
} from '../actions/index';

class Players extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            choosenUser: '',
        };
        this.inputUser = React.createRef();
    }

    /**
 * Fetch list of available players to usersReducer.
 * @return {void}
 */
    UNSAFE_componentWillMount() { // eslint-disable-line
        this.props.getUsersFromStorage();
    }

    /**
 * Inserts value from input field.
 * @param {event object} event onInput event object.
 * @return {void}
 */
    handleInput(event) {
        this.userName = event.target.value;
    }

    /**
 * Function is calling when a user clicks on choosen player.
 * @param {number} i This is the player index.
 * @return {void}
 */
    handleUserClick(i) {
        const { players } = this.props;
        for (let j = 0; j < players.length; j += 1) {
            if (players[j].isSelected) {
                players[j].isSelected = false;
            }
        }
        players[i].isSelected = true;
        this.setState({
            choosenUser: players[i],
        });
        this.props.changeUsersPlaylist(players);
    }

    /**
 * Inserts new user to list. And creates new user in localStorage.
 * @return {void}
 */
    handleOnClickAddUser() {
        if (this.userName && this.userName.length > 0) {
            this.props.saveUserToStorage({
                name: this.userName,
                id: (this.props.players.length + 1),
            });
            this.props.addNewUserToList(this.userName);
            this.inputUser.current.value = '';
        }
    }

    /**
 * If the user is selected game starts.
 * @return {void}
 */
    handleOnClickPlay() {
        if (this.state.choosenUser) {
            this.props.fetchUserFromStorage(this.state.choosenUser.id);
            this.props.history.push('/game');
        }
    }

    /**
 * Render players list from state.users.
 * @return {<html>}
 */
    renderPlayers() {
        if (!this.props.players) {
            return '';
        }
        const { players } = this.props;
        const arr = [];
        for (let i = 0; i < players.length; i += 1) {
            if (!players[i]) {
                return '';
            }
            arr.push(<div onClick={() => {
                this.handleUserClick(i);
            }} className={classNames({
                'list-element': true,
                selected: players[i].isSelected,
            })} id={players[i].id} key={i}>{players[i].name}</div>);
        }
        return (
            arr
        );
    }

    render() {
        return (
            <div className='players-wrap'>
                <div className='content-wrap'>
                    <div className='choose-player'>Choose player or add new</div>
                    <div className='list-wrap'>
                        <div className='choosen-user'>{ this.state.choosenUser.name }</div>
                        <div className='list'>
                            { this.renderPlayers() }
                        </div>
                    </div>
                    <div className='add-wrap'>
                        <input ref={this.inputUser} className='input-user' onInput={(e) => {
                            this.handleInput(e);
                        }} />
                        <button className='play-button button' onClick={() => {
                            this.handleOnClickPlay();
                        }}>Go</button>
                        <button className='add-user-button button' onClick={() => {
                            this.handleOnClickAddUser();
                        }}>Add</button>
                    </div>
                </div>
            </div>
        );
    }
}
Players.propTypes = {
    getUsersFromStorage: PropTypes.func,
    changeUsersPlaylist: PropTypes.func,
    addNewUserToList: PropTypes.func,
    saveUserToStorage: PropTypes.func,
    fetchUserFromStorage: PropTypes.func,
    players: PropTypes.array,
    history: PropTypes.object,
};

const mapStateToProps = state => ({
    players: state.users,
});

const mapDispatchToProps = {
    getUsersFromStorage,
    changeUsersPlaylist,
    addNewUserToList,
    saveUserToStorage,
    fetchUserFromStorage,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Players);
