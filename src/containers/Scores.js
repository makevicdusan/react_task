import React from 'react';
import '../css/Scores.css';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { levelScoresIsOpen, selectLevel } from '../actions/index';


class Scores extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            levelScores: {
                level: '',
                scores: [],
            },
            offset: '',
            limit: '',
            scores: this.props.user.scores.slice().reverse(),
        };
    }

    /**
 * If in score you have more than ten levels set limit 10.
 * @return {void}
 */
    UNSAFE_componentWillMount() { // eslint-disable-line
        if (this.state.scores.length > 10) {
            this.setState({
                offset: 0,
                limit: 10,
            });
        } else {
            this.setState({
                offset: 0,
                limit: this.state.scores.length,
            });
        }
    }

    /**
 *  When player click on left icon users changes which level to see.
 * @return {void}
 */
    handleOnClickLeftPag() {
        if (this.state.offset > 0) {
            if (this.state.limit % 10 !== 0) {
                this.setState({
                    offset: this.state.offset - 10,
                    limit: this.state.limit - (this.state.limit % 10),
                });
            } else {
                this.setState({
                    offset: this.state.offset - 10,
                    limit: this.state.limit - 10,
                });
            }
        }
    }

    /**
 *  When player click on right icon users changes which level to see.
 * @return {void}
 */
    handleOnClickRightPag() {
        const scoresLen = this.state.scores.length;
        if ((scoresLen - 10) > 10) {
            this.setState({
                offset: this.state.offset + 10,
                limit: this.state.limit + 10,
            });
        } else if ((scoresLen - 10) < 10 && (scoresLen - 10) > 0) {
            this.setState({
                offset: this.state.offset + 10,
                limit: this.state.limit + (scoresLen - 10),
            });
        }
    }

    /**
 * Click on "icon plus" next to top score opens more score of that level.
 * @return {void}
 */
    handleOnClickOpenOtherScores(levelScores) {
        if (levelScores.isOpen) {
            this.props.levelScoresIsOpen({
                levelScores,
                isOpen: false,
            });
        } else {
            this.props.levelScoresIsOpen({
                levelScores,
                isOpen: true,
            });
        }
    }

    /**
 *  Function calls when player click on one level in table.
 * Than row is selected and chart for that row is shown.
 * @return {void}
 */
    handleRowOnClick(i) {
        this.props.selectLevel(i);
        this.addScoresCanvas(this.props.user.scores[i]);
    }

    /**
 * Render other score when player call "handleOnClickOpenOtherScores" function.
 * @return {<html>}
 */
    renderOtherScores(otherScores, levelIsOpen) { // eslint-disable-line
        if (otherScores.length < 1 || !levelIsOpen) {
            return '';
        }
        return (
            <div className='other-scores'>
                { otherScores.map((o, i) => (
                    <div className='other-score' key={`${i}other`}>{ o } seconds</div>
                ))}
            </div>
        );
    }

    /**
 * Iterate through all scores and show them in the table.
 * @return {<html>}
 */
    renderScores() {
        const { scores } = this.state;
        const arr = [];
        for (let i = this.state.offset; i < this.state.limit; i += 1) {
            const { games } = scores[i];
            let topScore = Number.MAX_SAFE_INTEGER;
            const otherScores = [];
            let topScoreIndex = '';
            for (let j = 0; j < games.length; j += 1) {
                otherScores.push(games[j].timeLast);
                if (games[j].timeLast < topScore) {
                    topScore = games[j].timeLast;
                    topScoreIndex = j;
                }
            }
            topScore = parseInt(topScore, 10);
            otherScores.splice(topScoreIndex, 1);
            if (i === this.state.offset) {
                arr.push(
                    <div key='header' className='table-row-header'>
                        <div>Level</div><div>Time</div><div>Times completed</div>
                    </div>,
                );
            }
            arr.push(
                <div onClick={(e) => {
                    e.stopPropagation();
                    this.handleRowOnClick(scores[i].level - 1);
                }} key={i} className={classNames({
                    'table-row': true,
                    'selected-row': scores[i].isSelected,
                })}>
                    <div className='table-column'>{ scores[i].level }</div>
                    <div className='table-column-open'>
                        <div className={classNames({
                            'top-score': true,
                            'first-row': (i === (scores.lenght - 1)),
                        })}>
                            { topScore } seconds
                            <img onClick={() => {
                                this.handleOnClickOpenOtherScores(scores[i]);
                            }} className={classNames({
                                'icon-image': true,
                                'not-see': scores[i].isOpen,
                            })} src='assets/images/plus-icon.png' alt='plus'/>
                            <img onClick={() => {
                                this.handleOnClickOpenOtherScores(scores[i]);
                            }}
                            className={classNames({
                                'icon-image': true,
                                'not-see': !scores[i].isOpen,
                            })}
                            src='assets/images/negative.png' alt='minus'/>
                        </div>
                        { this.renderOtherScores(otherScores, scores[i].isOpen) }
                    </div>
                    <div className='table-column'>{ scores[i].games.length }</div>
                </div>,
            );
        }
        return (
            arr
        );
    }

    /**
 * Function calculates where chart lines are on the canvas.
 * Also take two canvas helper to draw lines that indicate click and seconds need to click.
 * @return {void}
 */
    addScoresCanvas(levelScore) { // eslint-disable-line
        const level = levelScore;
        const average = [];
        for (let i = 0; i < level.games.length; i += 1) {
            for (let j = 0; j < level.games[i].clicks.length; j += 1) {
                if (i === 0) {
                    average[j] = {
                        click: (j + 1),
                        avg: level.games[i].clicks[j].seconds,
                    };
                } else {
                    average[j].avg += level.games[i].clicks[j].seconds;
                }
                if (i === (level.games.length - 1)) {
                    average[j].avg /= level.games.length;
                    average[j].avg = Number(average[j].avg);
                }
            }
        }
        let biggestAvg = 0;
        const biggestClick = average.length;
        for (let i = 0; i < average.length; i += 1) {
            if (i !== 0 && (average[i].avg - average[i - 1].avg) > biggestAvg) {
                biggestAvg = (average[i].avg - average[i - 1].avg);
            } else if (i === 0) {
                biggestAvg = average[i].avg;
            }
        }
        for (let i = 0; i < average.length; i += 1) {
            if (i === 0) {
                average[i].placeY = ((200 - (100 / biggestAvg) * average[i].avg));
                average[i].placeX = ((240 / biggestClick) * average[i].click);
            } else {
                average[i].placeY = (200 - (100 / biggestAvg) * (average[i].avg - average[i - 1].avg)); // eslint-disable-line
                average[i].placeX = ((240 / biggestClick) * average[i].click);
            }
        }
        const c = document.getElementById('graphic');
        const ctx = c.getContext('2d');
        ctx.clearRect(0, 0, c.width, c.height);
        // draw
        let timesDrawY;
        let timesDrawX;
        if (biggestAvg < 5) {
            timesDrawY = biggestAvg;
        } else {
            timesDrawY = 5;
        }
        if (biggestClick < 5) {
            timesDrawX = biggestClick;
        } else {
            timesDrawX = 5;
        }
        const y = document.getElementById('apc');
        const yC = y.getContext('2d');
        yC.clearRect(0, 0, y.width, y.height);
        // draw y numbers
        for (let i = 1; i <= Math.round(timesDrawY); i += 1) {
            ctx.beginPath();
            ctx.lineWidth = '2';
            ctx.strokeStyle = 'violet';
            const positionY = Math.round(((200 - (100 / biggestAvg) * ((biggestAvg / timesDrawY) * i)))); // eslint-disable-line

            ctx.moveTo(0, positionY);
            ctx.lineTo(10, positionY);
            ctx.font = '20px Arial';
            ctx.fillStyle = 'lightblue';
            ctx.fillText((Math.round(((biggestAvg / timesDrawY) * i))).toString(), 10, positionY + 10); // eslint-disable-line
            ctx.stroke();

            yC.beginPath();
            yC.lineWidth = '2';
            yC.strokeStyle = 'violet';
            yC.moveTo(10, positionY);
            yC.lineTo(20, positionY);
            yC.stroke();
        }

        const x = document.getElementById('ord');
        const xC = x.getContext('2d');
        xC.clearRect(0, 0, x.width, x.height);
        // draw x numbers
        for (let i = 1; i <= Math.round(timesDrawX); i += 1) {
            if (i === Math.round()) ctx.beginPath();
            ctx.lineWidth = '2';
            ctx.strokeStyle = 'violet';
            const positionX = Math.round(((240 / biggestClick) * ((biggestClick / timesDrawX) * i))); // eslint-disable-line
            ctx.moveTo(positionX, 190);
            ctx.lineTo(positionX, 200);
            ctx.stroke();

            xC.beginPath();
            xC.lineWidth = '2';
            xC.strokeStyle = 'violet';
            xC.moveTo(positionX, 0);
            xC.lineTo(positionX, 10);
            xC.font = '20px Arial';
            xC.fillStyle = 'lightblue';
            xC.fillText((Math.round(((biggestClick / timesDrawX) * i))).toString(), positionX, 20);
            xC.stroke();
        }


        // draw y and x
        ctx.beginPath();
        ctx.lineWidth = '4';
        ctx.strokeStyle = 'violet';
        ctx.moveTo(0, 0);
        ctx.lineTo(0, 200);
        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = '4';
        ctx.strokeStyle = 'violet';
        ctx.moveTo(0, 200);
        ctx.lineTo(300, 200);
        ctx.stroke();
        // draw y and x

        for (let i = 0; i < average.length; i += 1) {
            if (i === 0) {
                ctx.beginPath();
                ctx.lineWidth = '1';
                ctx.strokeStyle = 'white';
                ctx.moveTo(0, 200);
                ctx.lineTo(average[i].placeX, average[i].placeY);
                ctx.arc(average[i].placeX, average[i].placeY, 5, 0, 2 * Math.PI);
                ctx.stroke();
            } else {
                ctx.beginPath();
                ctx.lineWidth = '1';
                ctx.strokeStyle = 'white';
                ctx.moveTo(average[i - 1].placeX, average[i - 1].placeY);
                ctx.lineTo(average[i].placeX, average[i].placeY);
                ctx.arc(average[i].placeX, average[i].placeY, 5, 0, 2 * Math.PI);
                ctx.stroke();
            }
        }
    }

    render() {
        return (
            <div className='scores-wrap'>
                <div className='scores-content-wrap'>
                    <div className='score-table'>
                        { this.renderScores() }
                        <div className={classNames({
                            'table-pagination': true,
                            'table-pagination-none': (this.props.user.scores.length < 10),
                        })}><span><img onClick={(e) => {
                                e.stopPropagation();
                                this.handleOnClickLeftPag();
                            }} className='icon-image' src='assets/images/left-arrow.png' alt='left'/></span>
                            <span><img onClick={(e) => {
                                e.stopPropagation();
                                this.handleOnClickRightPag();
                            }} className='icon-image' src='assets/images/right-arrow.png' alt='right'/></span>
                        </div>
                    </div>
                    <div className='score-chart'>
                        <div className='canvas-wrap'>
                            <canvas id='apc' width="20" height="200"></canvas>
                            <canvas id="graphic" width="300" height="200"></canvas>
                            <div className='between-canvas'></div>
                            <canvas id='ord' width="300" height="20"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Scores.propTypes = {
    user: PropTypes.object,
    game: PropTypes.object,
    timer: PropTypes.number,
    boxes: PropTypes.number,
    levelScoresIsOpen: PropTypes.func,
    selectLevel: PropTypes.func,
};

const mapStateToProps = state => ({
    user: state.user,
    game: state.game,
    timer: state.timer,
    boxes: state.boxes,
});

const mapDispatchToProps = {
    levelScoresIsOpen,
    selectLevel,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Scores);
