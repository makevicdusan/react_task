import React from 'react';
import '../css/Table.css';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    changeStatus, setBoxes, changeLevel, addClick,
    addInterval, removeInterval,
    resetTimer, addTimer, chooseLevel, addLife,
    minusLifes, addScore,
} from '../actions/index';
import { Status } from '../reducers/gameReducer';
import config from '../config/config';

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            matrix: [],
            status: Status.READY,
        };
        this.generatedFields = [];
        this.clickedFields = [];
        this.fieldsThatCanClick = [];
        this.numClicks = 0;
        this.currentGameScore = {
            timeLast: null,
            clicks: [],
        };
    }

    /**
 * Fill table field objects in state.
 * @return {void}
 */
    componentDidMount() {
        this.fillTableFields();
    }

    fillTableFields() {
        const matrix = [];
        let fKey = 0;
        for (let i = 0; i < 10; i += 1) {
            matrix[i] = [];
            for (let j = 0; j < 10; j += 1) {
                matrix[i].push({
                    i, j, fKey, canClick: false, generated: false,
                });
                fKey += 1;
            }
        }
        this.setState({ matrix });
    }

    /**
 * Returns fields available to click by set pattern.
 * @param {object} field The field with whom it calculates pattern.
 * @return {Array}
 */
    returnAvailableFields(field) { // eslint-disable-line
        const availableFields = [];
        if (field.i - 3 > -1) {
            availableFields.push({ i: (field.i - 3), j: field.j });
        }
        if ((field.i + 3) < 10) {
            availableFields.push({ i: (field.i + 3), j: field.j });
        }
        if ((field.j - 3) > -1) {
            availableFields.push({ i: field.i, j: (field.j - 3) });
        }
        if ((field.j + 3) < 10) {
            availableFields.push({ i: field.i, j: (field.j + 3) });
        }
        if (((field.i - 2) > -1) && ((field.j + 2) < 10)) {
            availableFields.push({ i: (field.i - 2), j: (field.j + 2) });
        }
        if (((field.i - 2) > -1) && ((field.j - 2) > -1)) {
            availableFields.push({ i: (field.i - 2), j: (field.j - 2) });
        }
        if (((field.i + 2) < 10) && ((field.j + 2) < 10)) {
            availableFields.push({ i: (field.i + 2), j: (field.j + 2) });
        }
        if (((field.i + 2) < 10) && ((field.j - 2) > -1)) {
            availableFields.push({ i: (field.i + 2), j: (field.j - 2) });
        }
        return availableFields;
    }

    /**
 * Remove color from fields.
 * @param {Array} fields Fields from which to remove the color.
 * @param {string} property Property that gives color in classNames().
 * It can be canClick, clicked, generated, firstField.
 * @return {void}
 */
    removeColorFromFields(fields, property) {
        const { matrix } = this.state;
        for (let z = 0; z < fields.length; z += 1) {
            matrix[fields[z].i][fields[z].j][property] = false;
        }
    }

    /**
 * Add color to fields.
 * @param {Array} fields Fields where we add color.
 * @param {string} property Property that gives color in classNames().
 * It can be canClick, clicked, generated, firstField.
 * @return {void}
 */
    colorFields(fields, property) {
        const { matrix } = this.state;
        for (let z = 0; z < fields.length; z += 1) {
            if (fields[z] && matrix[fields[z].i][fields[z].j]) {
                matrix[fields[z].i][fields[z].j][property] = true;
            }
        }
        this.setState({
            matrix,
        });
    }

    /**
* Return random number from min to max inclusively.
* @param {number} min
* @param {number} max
* @return {number}
*/
    generateRandomNum(min, max) { //eslint-disable-line
        const min1 = Math.ceil(min);
        const max1 = Math.floor(max);
        return Math.floor(Math.random() * ((max1 - min1) + 1)) + min1;
    }

    /**
 * An algorithm that generates the fields to be clicked on.
 * @param {object} field Current field.
 * @param {Array} generatedFields Already generated fields.
 * @param {number} startLevel How many fields need to be depends on level, level === fields.
 * @return {void}
 */
    generateRecursive(startField, generatedFields, startLevel) {
        if (startLevel <= 0 || !startField) {
            return;
        }
        const availableFields = this.returnAvailableFields(startField);
        const level = startLevel;
        for (let i = 0; i < generatedFields.length; i += 1) {
            for (let j = 0; j < availableFields.length; j += 1) {
                if (generatedFields[i].i === availableFields[j].i
        && generatedFields[i].j === availableFields[j].j) {
                    availableFields.splice(j, 1);
                    j -= 1;
                }
            }
        }
        if (availableFields.length < 1) {
            const newFields = [];
            let availableFl;
            for (let i = 0; i < generatedFields.length; i += 1) {
                availableFl = this.returnAvailableFields(generatedFields[i]);
                if (availableFl.length > 0) {
                    newFields.push(generatedFields[i]);
                }
            }
            const randNum = this.generateRandomNum(0, newFields.length - 1);
            this.generateRecursive(generatedFields[randNum], generatedFields, level);
        }
        const randomNum = this.generateRandomNum(0, (availableFields.length - 1));
        if (availableFields[randomNum]) {
            generatedFields.push(availableFields[randomNum]);
        }
        this.generateRecursive(availableFields[randomNum], generatedFields, (level - 1));
    }

    /**
 * Returns fields that players can click on during the game.
 * Result is cross-section these two arrays.
 * @param {Array} generatedFields Already generated fields.
 * @param {Array} availableFields  All fields that user can click.
 * @return {Array}
 */
    returnFieldsThatCanClick(generatedFields, availableFields) {
        const arr = [];
        const { matrix } = this.state;
        for (let i = 0; i < generatedFields.length; i += 1) {
            for (let j = 0; j < availableFields.length; j += 1) {
                if (generatedFields[i].i === availableFields[j].i
        && generatedFields[i].j === availableFields[j].j
        && (!matrix[availableFields[j].i][availableFields[j].j].clicked
          && !matrix[availableFields[j].i][availableFields[j].j].firstField)) {
                    arr.push(availableFields[j]);
                }
            }
        }
        return arr;
    }

    /**
 * Removes field that is just clicked from fields that can be clicked.
 * @param {object} field The field from which it is removed.
 * @return {void}
 */
    removeCurrentFieldFromThatCanClick(field) {
        for (let i = 0; i < this.fieldsThatCanClick.length; i += 1) {
            if (this.fieldsThatCanClick.i === field.i && this.fieldsThatCanClick.j === field.j) {
                this.fieldsThatCanClick.splice(i, 1);
            }
        }
    }

    /**
 * Calculates which level user is going to play.
 * @return {number}
 */
    returnGameLevel() {
        let startLevel;
        if (this.props.game.level
      && this.props.game.level <= this.props.user.currentLevel) {
            startLevel = this.props.game.level;
        } else {
            startLevel = this.props.user.currentLevel;
        }
        if (config && config.startLevel && startLevel < config.startLevel) {
            this.props.chooseLevel(config.startLevel);
            return Number(config.startLevel);
        }
        this.props.chooseLevel(startLevel);
        return Number(startLevel);
    }

    /**
 * Starts timer interval to add seconds.
 * @return {void}
 */
    startInterval() {
        const interval = setInterval(() => {
            this.props.addTimer();
        }, 1000);
        this.props.addInterval(interval);
    }

    /**
 * Remove all properties from matrix except of positions.
 * @return {void}
 */
    resetMatrix() {
        const { matrix } = this.state;
        for (let i = 0; i < matrix.length; i += 1) {
            for (let j = 0; j < matrix[i].length; j += 1) {
                matrix[i][j].canClick = false;
                matrix[i][j].generated = false;
                matrix[i][j].clicked = false;
                matrix[i][j].firstField = false;
            }
        }
    }

    /**
 * If the status is READY changes status to RUNNING and begins game.
 * If status is RUNNING color already clicked fields, color fields that user can click on,
 * decreases fields(boxes) left, adds click which we need for chart.
 * If game is won reset game and insert this game data to user scores.
 * @param {object} field Clicked field.
 * @return {void}
 */
    handleOnClick(field) {
        if (!this.props.user.currentLevel) {
            this.props.history.push('/');
            return;
        }
        let availableFields;
        if (this.props.game.status === Status.READY) {
            this.startInterval();
            const gameLevel = this.returnGameLevel();
            this.props.setBoxes(gameLevel);
            this.generatedFields.push(field);
            availableFields = this.returnAvailableFields(field);
            this.generateRecursive(field, this.generatedFields, this.returnGameLevel());
            this.colorFields(this.generatedFields, 'generated');
            this.fieldsThatCanClick = this.returnFieldsThatCanClick(this.generatedFields, availableFields); // eslint-disable-line
            this.colorFields(this.fieldsThatCanClick, 'canClick');
            this.props.changeStatus(Status.RUNNING);
            field.firstField = true;
            field.generated = false;
        } else if (this.props.game.status === Status.RUNNING) {
            if (!field.canClick) {
                return;
            }
            this.numClicks += 1;
            this.props.setBoxes(this.returnGameLevel() - this.numClicks);
            availableFields = this.returnAvailableFields(field);

            if (field.canClick && field.generated && !field.firstField && !field.clicked) {
                this.currentGameScore.clicks.push(
                    {
                        seconds: this.props.timer,
                        numOfClick: this.numClicks,
                    },
                );
            }
            this.removeCurrentFieldFromThatCanClick(field);
            this.removeColorFromFields(this.fieldsThatCanClick, 'canClick');
            this.fieldsThatCanClick = this.returnFieldsThatCanClick(this.generatedFields, availableFields); // eslint-disable-line
            if (this.fieldsThatCanClick.length === 0 && this.props.boxes > 1) {
                const numOfLifes = this.props.user.lifes - this.props.boxes;
                if (numOfLifes <= 0) {
                    this.props.changeStatus(Status.GAMEOVER);
                    this.props.minusLifes(this.props.boxes - 1);
                    this.props.changeLevel(1);
                    this.props.chooseLevel(1);
                    this.props.setBoxes(0);
                    this.currentGameScore = {
                        timeLast: null,
                        clicks: [],
                    };
                    setTimeout(() => {
                        this.resetGame(Status.READY);
                    }, 3000);
                } else {
                    this.props.changeStatus(Status.LOSE);
                    this.props.minusLifes(this.props.boxes - 1);
                    this.props.setBoxes(0);
                    this.currentGameScore = {
                        timeLast: null,
                        clicks: [],
                    };
                    setTimeout(() => {
                        this.resetGame(Status.READY);
                    }, 2000);
                }
                return;
            }
            this.colorFields(this.fieldsThatCanClick, 'canClick');
            field.clicked = true;
            if (this.props.boxes === 1) {
                this.currentGameScore.timeLast = this.props.timer;
                this.props.changeStatus(Status.WIN);
                const payload = {
                    level: this.returnGameLevel(),
                    gameScore: this.currentGameScore,
                };
                this.props.addScore(payload);
                this.currentGameScore = {
                    timeLast: null,
                    clicks: [],
                };
                const currentLevel = Number(this.returnGameLevel());
                const nextLevel = Number(currentLevel + 1);

                this.props.changeLevel(nextLevel);
                this.props.chooseLevel(nextLevel);
                this.props.addLife();
                setTimeout(() => {
                    this.resetGame(Status.READY);
                }, 1000);
            }
        }
    }

    /**
 * Reset all game data to initial.
 * @param {number} status Status to change game to.
 * @return {void}
 */
    resetGame(status) {
        this.resetMatrix();
        this.props.removeInterval();
        this.props.resetTimer();
        this.props.changeStatus(status);
        this.generatedFields = [];
        this.availableFields = [];
        this.fieldsThatCanClick = [];
        this.numClicks = 0;
    }

    /**
 * Render little popup which shows on game PAUSED, GAMEOVER, LOSE, FINISHED.
 * @return {<html>}
 */
    renderLittlePopup() {
        let message = '';
        let didRender = false;
        if (this.props.game.status === Status.PAUSED) {
            message = 'Paused';
            didRender = true;
        } else if (this.props.game.status === Status.GAMEOVER) {
            message = 'Gameover';
            didRender = true;
        } else if (this.props.game.status === Status.LOSE) {
            message = 'You lose!';
            didRender = true;
        } else if (this.props.game.status === Status.FINISHED) {
            message = 'You finish a game. Congratulations!!!!!!!!';
            didRender = true;
        }
        if (didRender) {
            return (
                <div className='layer-above'>
                    <div className='little-popup'>
                        <div>{ message }</div>
                    </div>
                </div>
            );
        }
        return '';
    }

    render() {
        return (
            <div className='table-wrap'>
                { this.renderLittlePopup() }
                <div className='table'>{
                    this.state.matrix.map((row, index) => (
                        <div className='table-row' key={index}>
                            {
                                row.map(field => (
                                    <div onClick={() => this.handleOnClick(field)} field={field}
                                        className={classNames({
                                            field: true,
                                            'field-can-click': field.canClick,
                                            'field-generated': field.generated,
                                            'field-clicked': field.clicked,
                                            'first-field': field.firstField,
                                        })} key={field.fKey}>
                                    </div>
                                ))
                            }
                        </div>
                    ))}</div>
            </div>
        );
    }
}

Table.propTypes = {
    game: PropTypes.object,
    user: PropTypes.object,
    timer: PropTypes.number,
    boxes: PropTypes.number,
    changeStatus: PropTypes.func,
    setBoxes: PropTypes.func,
    changeLevel: PropTypes.func,
    addClick: PropTypes.func,
    addInterval: PropTypes.func,
    removeInterval: PropTypes.func,
    resetTimer: PropTypes.func,
    addTimer: PropTypes.func,
    chooseLevel: PropTypes.func,
    addLife: PropTypes.func,
    minusLifes: PropTypes.func,
    addScore: PropTypes.func,
    history: PropTypes.object,
};

const mapStateToProps = state => ({
    user: state.user,
    game: state.game,
    timer: state.timer,
    boxes: state.boxes,
});

const mapDispatchToProps = {
    changeStatus,
    setBoxes,
    changeLevel,
    addClick,
    addInterval,
    removeInterval,
    resetTimer,
    addTimer,
    chooseLevel,
    addLife,
    minusLifes,
    addScore,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Table);
