import React from 'react';
import '../css/Choose.css';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    changeStatus, chooseLevel, addInterval, removeInterval,
    addTimer, saveExistingUserToStorage,
} from '../actions/index';
import config from '../config/config';
import { Status } from '../reducers/gameReducer';


class Choose extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            selectValue: '',
        };
    }

    /**
 * If status is RUNNING pauses the game and shows the score.
 * If status is PAUSED returns to the game.
 * If status is ready changes status to SCORE and shows score.
 * If status is score returns status to READY.
 * @return {void}
 */
    handleOnClick() {
        const { status } = this.props.game;
        if (status === Status.RUNNING) {
            this.returnStatus = this.props.game.status;
            this.props.changeStatus(Status.PAUSED);
            if (this.returnStatus === Status.RUNNING) {
                this.props.removeInterval();
            }
        } else if (status === Status.PAUSED) {
            this.props.changeStatus(this.returnStatus);
            if (this.returnStatus === Status.RUNNING) {
                this.props.addInterval(setInterval(() => {
                    this.props.addTimer();
                }, 1000));
            }
        }
        if (status === Status.READY) {
            this.props.changeStatus(Status.SCORE);
        } else if (status === Status.SCORE) {
            this.props.changeStatus(Status.READY);
        }
    }

    /**
 * Function calls when user choose level to play.
 * @param {event object} event onClick event object.
 * @return {void}
 */
    handleChange(event) {
        if (this.props.game.status === Status.READY) {
            const value = event.target.value; // eslint-disable-line
            this.props.chooseLevel(value);
        }
    }

    /**
 * Return user to page players to choose another player.
 * @return {void}
 */
    backChooseUser() {
        if (this.props.game.status !== Status.RUNNING) {
            this.props.saveExistingUserToStorage();
            this.props.changeStatus(Status.READY);
            this.props.history.push('/');
        }
    }

    /**
 * Renders options what level user can play.
 * @return {<html>}
 */
    renderSelectOptions() {
        let min = 1;
        let max = this.props.user.currentLevel;
        if (config && config.startLevel) {
            min = config.startLevel;
        }
        if (max < min) {
            max = min;
        }
        const arr = [];
        for (let i = min; i <= max; i += 1) {
            arr.push(<option value={i} key={i}>{i}</option>);
        }
        return arr;
    }

    render() {
        return (
            <div className='choose-level-wrap'>
                <div className='choose-level-items'>
                    { this.props.game.currentLevel }
                    <div> Choose level:
                        <select className='select-level' onClick={(e) => {
                            this.handleChange(e);
                        }}>
                            { this.renderSelectOptions().map(option => option) }
                        </select>
                    </div>
                    <div><button onClick={() => {
                        this.backChooseUser();
                    }} className='choose-button button'>Back to players</button></div>
                    <div><button onClick={() => this.handleOnClick()} className='choose-button button'>Pause/Score</button></div>
                </div>
            </div>
        );
    }
}

Choose.propTypes = {
    changeStatus: PropTypes.func,
    game: PropTypes.object,
    history: PropTypes.object,
    removeInterval: PropTypes.func,
    addInterval: PropTypes.func,
    addTimer: PropTypes.func,
    chooseLevel: PropTypes.func,
    saveExistingUserToStorage: PropTypes.func,
    user: PropTypes.object,
};

const mapStateToProps = state => ({
    game: state.game,
    user: state.user,
});

const mapDispatchToProps = {
    changeStatus,
    chooseLevel,
    addInterval,
    removeInterval,
    addTimer,
    saveExistingUserToStorage,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Choose);
