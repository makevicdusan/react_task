import React from 'react';
import '../css/Game.css';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Table from './Table';
import Stats from '../components/Stats';
import Scores from './Scores';
import Choose from './Choose';
import { Status } from '../reducers/gameReducer';
import { saveExistingUserToStorage } from '../actions/index';

class Game extends React.Component {
    componentDidMount() {
        window.addEventListener('beforeunload', () => {
            this.props.saveExistingUserToStorage();
        });
    }

    /**
  * Checks did user exist before mount, if not returns to page players.
  */
    UNSAFE_componentWillMount() { // eslint-disable-line
        let hasProp = false;
        const userString = JSON.stringify(this.props.user);
        if (userString.length > 3) {
            hasProp = true;
        }
        if (!hasProp) {
            this.props.history.push('/');
        }
    }

    /**
  * Save current user to localStorage before component destroy.
  */
    componentWillUnmount() {
        this.props.saveExistingUserToStorage();
    }

    /**
 * Function renders Scores component when status is FINISHED, GAMEOVER, SCORE or PAUSED
 * @return {<html>}
 */
    renderScores() {
        const { status } = this.props.game;
        if (status === Status.PAUSED || status === Status.FINISHED
      || status === Status.GAMEOVER || status === Status.SCORE) {
            return (
                <Scores onClick={(e) => {
                    e.preventDefault();
                }}/>
            );
        }
        return '';
    }

    /**
 * On mobile devices component Scores is on the bottom of screen.
 * @return {<html>}
 */
    render() {
        if (window.innerWidth < 992 && (window.innerWidth < window.innerHeight)) {
            return (
                <div className='game-wrap'>
                    <Choose history={this.props.history}/>
                    <div className='game-table-scores'>
                        <Table history={this.props.history} />
                    </div>
                    <Stats timer={this.props.timer} level={this.props.game.level}
                        lives={this.props.user.lifes} boxes={this.props.boxes}/>
                    { this.renderScores() }
                </div>
            );
        }
        return (
            <div className='game-wrap'>
                <Choose history={this.props.history}/>
                <div className='game-table-scores'>
                    <Table history={this.props.history} />
                    { this.renderScores() }
                </div>
                <Stats timer={this.props.timer} level={this.props.game.level}
                    lives={this.props.user.lifes} boxes={this.props.boxes}/>
            </div>
        );
    }
}

Game.propTypes = {
    saveExistingUserToStorage: PropTypes.func,
    user: PropTypes.object,
    history: PropTypes.object,
    game: PropTypes.object,
    timer: PropTypes.number,
    boxes: PropTypes.number,
};

const mapStateToProps = state => ({
    user: state.user,
    game: state.game,
    timer: state.timer,
    boxes: state.boxes,
});

const mapDispatchToProps = {
    saveExistingUserToStorage,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Game);
