import React from 'react';
import { Route } from 'react-router-dom'; // eslint-disable-line
import Game from '../containers/Game';
import '../css/Main.css';
import Players from '../containers/Players';

export default class Main extends React.Component {
    render() { // eslint-disable-line
        return (
            <div className='main-wrap'>
                <Route path='/game' component={Game}/>
                <Route exact path='/' component={Players} />
            </div>
        );
    }
}
