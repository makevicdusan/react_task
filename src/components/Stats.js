import React from 'react';
import '../css/Stats.css';

export default class Stats extends React.Component {
    render() {
        const { props } = this;
        return (
            <div className='stats-wrap'>
                <div className='stats-items'>
                    <div>Time: {props.timer}</div>
                    <div>Level: {props.level}</div>
                    <div>Lives: {props.lives}</div>
                    <div>Fields left: {props.boxes}</div>
                </div>
            </div>
        );
    }
}
